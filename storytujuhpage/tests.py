from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import storytujuhpage
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class UnitTest(TestCase):
    def test_apakah_url_bisa_diakses(self):
        response = Client().get('//')
        self.assertEqual(response.status_code, 200)

    def test_url_memanggil_storytujuhpageviews(self):
        found = resolve('/')
        self.assertEqual(found.func, storytujuhpage)

    def test_views_memanggil_html_storytujuhpage(self):
        response = Client().get('//')
        self.assertTemplateUsed(response, 'Story7.html')

    def test_apakah_ada_tulisan_annisya(self):
        response = Client().get('//')
        content = response.content.decode('utf8')
        self.assertIn("annisya", content)

    def test_apakah_ada_tulisan_deskripsi(self):
        response = Client().get('//')
        content = response.content.decode('utf8')
        self.assertIn("I'm a Information Systems student, who loves coffee and travelling so much. I’m also optimistic person that always believe in my dream and really hard to break down.", content) 

    def test_apakah_ada_tulisan_motto(self):
        response = Client().get('//')
        content = response.content.decode('utf8')
        self.assertIn("Dont Let Your Dreams be Dreams.", content)  

    def test_apakah_ada_tulisan_ubah_tema(self):
        response = Client().get('//')
        content = response.content.decode('utf8')
        self.assertIn("Change the theme:", content)

    def test_apakah_ada_tulisan_ubah_abot_me(self):
        response = Client().get('//')
        content = response.content.decode('utf8')
        self.assertIn("About Me:", content)

    def test_apakah_ada_java_script(self):
        response = Client().get('//')
        content = response.content.decode('utf8')
        self.assertIn("<script", content) 

    def test_apakah_ada_tulisan_jquery(self):
        response = Client().get('//')
        content = response.content.decode('utf8')
        self.assertIn("https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js", content)

    def test_apakah_ada_button_lightmode(self):
        response = Client().get('//')
        content = response.content.decode('utf8')
        self.assertIn("<button", content) 
        self.assertIn("Light Mode", content) 

    def test_apakah_ada_button_darkmode(self):
        response = Client().get('//')
        content = response.content.decode('utf8')
        self.assertIn("<button", content) 
        self.assertIn("Dark Mode", content)   

    def test_apakah_ada_tulisan_aktivitas(self):
        response = Client().get('//')
        content = response.content.decode('utf8')
        self.assertIn("Activity", content)

    def test_apakah_ada_tulisan_aktivitas1(self):
        response = Client().get('//')
        content = response.content.decode('utf8')
        self.assertIn("1. Sibuk dikerjain oleh tugas.", content)  
        self.assertIn("2. Sibuk ngurusin kontingen.", content) 
        self.assertIn("3. Sibuk persiapan BETIS 2020.", content) 

    def test_apakah_ada_tulisan_pengalaman(self):
        response = Client().get('//')
        content = response.content.decode('utf8')
        self.assertIn("Experience", content)   

    def test_apakah_ada_tulisan_pengalaman1(self):
        response = Client().get('//')
        content = response.content.decode('utf8')
        self.assertIn("1. BEM Fasilkom 2019", content) 
        self.assertIn("2. FNB Compfest 11", content) 
        self.assertIn("3. PJ Mentor dan Peserta BETIS 2020", content) 

    def test_apakah_ada_tulisan_prestasi(self):
        response = Client().get('//')
        content = response.content.decode('utf8')
        self.assertIn("Achievement", content) 

    def test_apakah_ada_tulisan_prestasi1(self):
        response = Client().get('//')
        content = response.content.decode('utf8')
        self.assertIn("1. Juara Umum Cerdas Cermat Matematika", content) 
        self.assertIn("2. Juara 2 Cerdas Cermat Umum Agama", content) 
        self.assertIn("3. Juara Tim Favorit Industriall Challenge Goes To School", content) 

    def test_apakah_ada_tulisan_terimakasih(self):
        response = Client().get('//')
        content = response.content.decode('utf8')
        self.assertIn("Thank you", content) 
        self.assertIn("and", content) 
        self.assertIn("see YAAA!!", content)

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()

    def test_input_status(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get(self.live_server_url)

        #cari elemen
        dark = selenium.find_element_by_id('buttondark')
        light = selenium.find_element_by_id('buttonlight')
        aktivitas = selenium.find_element_by_id('aktivitas')
        pengalaman = selenium.find_element_by_id('pengalaman')
        prestasi = selenium.find_element_by_id('prestasi')
        # klik tombol tema dark
        dark.send_keys(Keys.RETURN)
        # klik tombol tema light
        light.send_keys(Keys.RETURN)
        #expand accordion
        aktivitas.send_keys(Keys.RETURN)
        pengalaman.send_keys(Keys.RETURN)
        prestasi.send_keys(Keys.RETURN)
